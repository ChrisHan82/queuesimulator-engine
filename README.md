# QueueSimulator #

An adjustable queue simulator, written in Go.


### Usage: ###

There is a binary `main` in the `/src` directory, you can use it: 

`./main <#cutsomers> <#scanner queues> <#servers>`

For example, if you run `./main 50 2 4`, you will be simulating 50 customers, 2 scanner queues and 4 servers.

At the end of the program, it will show you the average waiting time among customers.




