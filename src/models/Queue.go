package models

import "fmt"

type Queue struct {
	Env            *Environment
	QueueID        int
	QueueType      string
	ClientsInQueue []*Client
}

type QueueError struct {
	errorStr string
}

func (qe *QueueError) Error() string {
	return qe.errorStr
}

func (q *Queue) Dequeue() (*Client, *QueueError) {
	if len(q.ClientsInQueue) == 0 {
		return nil, &QueueError{
			errorStr: fmt.Sprintf(
				"Queue #%d, type %s cannot dequeue. Queue is empty. \n", q.QueueID, q.QueueType),
		}
	}
	poppedClient := q.ClientsInQueue[0]
	poppedClient.Queue = nil
	q.ClientsInQueue = q.ClientsInQueue[1:]
	//fmt.Printf(
	//	"Client %d dequeued from queue #%d, type %s. \n", poppedClient.ClientID, q.QueueID, q.QueueType)
	return poppedClient, nil
}
