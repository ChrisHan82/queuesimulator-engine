package models

import (
	"fmt"
	rng "github.com/leesper/go_rng"
	"time"
)

type Server struct {
	Env                       *Environment
	ServerID                  int
	ServerTotalIdleTime       int
	ServerRecentIdleStartTime int
	ServingClient             *Client
}

type ServerError struct {
	errorStr string
}

func (se *ServerError) Error() string {
	return se.errorStr
}

func (s *Server) ServeLongestQueue() *ServerError {
	qs := s.Env.IDCheckQueuesInEnv
	if s.ServingClient != nil {
		return &ServerError{
			errorStr: fmt.Sprintf(
				"Server %d is currently serving client %d. \n", s.ServerID, s.ServingClient.ClientID),
		}
	}
	longestQ := qs[0]
	for _, q := range qs {
		if len(q.ClientsInQueue) > len(longestQ.ClientsInQueue) {
			longestQ = q
		}
	}
	if len(longestQ.ClientsInQueue) == 0 {
		//fmt.Printf(
		//	"The longest queue has 0 client to serve, server %d is still idle. \n", s.ServerID)
		return nil
	}
	s.ServerTotalIdleTime += s.Env.GlobalTime - s.ServerRecentIdleStartTime
	poppedClient, err := longestQ.Dequeue()
	if err != nil {
		return &ServerError{
			errorStr: err.errorStr,
		}
	}
	s.ServingClient = poppedClient
	//fmt.Printf(
	//	"Server %d is now serving client %d from queue #%d, type %s. \n",
	//	s.ServerID, poppedClient.ClientID, longestQ.QueueID, longestQ.QueueType)
	if err := s.serveClient(); err != nil {
		return &ServerError{
			errorStr: err.errorStr,
		}
	}
	s.ServingClient = nil
	s.ServerRecentIdleStartTime = s.Env.GlobalTime
	//fmt.Printf(
	//	"Server %d has completed serving client %d. \n",
	//	s.ServerID, poppedClient.ClientID)
	poppedClient.JoinShortestQueue("Scanner Queue")
	return nil
}

func (s *Server) serveClient() *ServerError {
	poissonRNG := rng.NewPoissonGenerator(time.Now().UnixNano())
	rn := int(poissonRNG.Poisson(13.3)) // Here use 1/0.75 * 10, because lib only give int
	nextInterval := int(float32(1000 * 10) / float32(rn))
	time.Sleep(time.Duration(nextInterval) * time.Millisecond)
	return nil
}
