package models

type Environment struct {
	ClientsInEnv       []*Client
	ServersInEnv       []*Server
	ScannersInEnv      []*Scanner
	IDCheckQueuesInEnv []*Queue
	ScannerQueuesInEnv []*Queue
	GlobalTime         int
}
