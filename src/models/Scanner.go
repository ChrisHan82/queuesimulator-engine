package models

import (
	"fmt"
	"time"
	rng "github.com/leesper/go_rng"
)

type Scanner struct {
	Env                        *Environment
	ScannerID                  int
	ScannerTotalIdleTime       int
	ScannerRecentIdleStartTime int
	ScanningClient             *Client
	ScannerBoundQueue          *Queue  // Each scanner is only bound to one specific queue
}

type ScannerError struct {
	errorStr string
}

func (se *ScannerError) Error() string {
	return se.errorStr
}

func (s *Scanner) ScanBoundQueue() *ScannerError {
	if s.ScanningClient != nil {
		return &ScannerError{
			errorStr: fmt.Sprintf(
				"Scanner %d is currently scanning client %d. \n", s.ScannerID, s.ScanningClient.ClientID),
		}
	}
	boundQueue := s.ScannerBoundQueue
	if len(boundQueue.ClientsInQueue) == 0 {
		//fmt.Printf(
		//	"The bound queue has 0 client to scan, scanner %d is still idle. \n", s.ScannerID)
		return nil
	}
	s.ScannerTotalIdleTime += s.Env.GlobalTime - s.ScannerRecentIdleStartTime
	poppedClient, err := boundQueue.Dequeue()
	if err != nil {
		return &ScannerError{
			errorStr: err.errorStr,
		}
	}
	s.ScanningClient = poppedClient
	//fmt.Printf(
	//	"Scanner %d is now scanning client %d from queue #%d, type %s. \n",
	//	s.ScannerID, poppedClient.ClientID, boundQueue.QueueID, boundQueue.QueueType)
	if err := s.scanClient(); err != nil {
		return &ScannerError{
			errorStr: err.errorStr,
		}
	}
	s.ScanningClient = nil
	s.ScannerRecentIdleStartTime = s.Env.GlobalTime
	//fmt.Printf(
	//	"Scanner %d has completed scanning client %d. \n",
	//	s.ScannerID, poppedClient.ClientID)
	poppedClient.ClientEndTime = s.Env.GlobalTime
	fmt.Printf(
		"Client %d has ended process. Spent %d total time. \n",
		poppedClient.ClientID, poppedClient.ClientEndTime - poppedClient.ClientStartTime)
	return nil
}

func (s *Scanner) scanClient() *ScannerError {
	uniformRNG := rng.NewUniformGenerator(time.Now().UnixNano())
	rn := uniformRNG.Int32Range(500, 1000)
	time.Sleep(time.Duration(rn) * time.Millisecond)
	return nil
}
