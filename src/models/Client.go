package models

import "fmt"

type Client struct {
	Env               *Environment
	ClientID          int
	ClientStartTime   int
	ClientEndTime     int
	Queue             *Queue
}

type ClientError struct {
	errorStr string
}

func (ce *ClientError) Error() string {
	return ce.errorStr
}


func (c *Client) JoinShortestQueue(queueType string) *ClientError {
	qs := make([]*Queue, 0)
	if queueType == "Boarding-Pass Check Queue" {
		qs = c.Env.IDCheckQueuesInEnv
	} else if queueType == "Scanner Queue" {
		qs = c.Env.ScannerQueuesInEnv
	} else {
		return &ClientError{
			errorStr: "Unknown queue type.",
		}
	}
	if len(qs) == 0 {
		return &ClientError{
			errorStr: fmt.Sprintf(
				"Client %d cannot join any %s. No Queue is available. \n", c.ClientID, queueType),
		}
	}
	if c.Queue != nil {
		return &ClientError{
			errorStr: fmt.Sprintf(
				"Client %d is already in queue #%d, type %s. \n", c.ClientID, c.Queue.QueueID, c.Queue.QueueType),
		}
	}
	shortestQ := qs[0]
	for _, q := range qs {
		if len(q.ClientsInQueue) < len(shortestQ.ClientsInQueue) {
			shortestQ = q
		}
	}
	shortestQ.ClientsInQueue = append(shortestQ.ClientsInQueue, c)
	c.Queue = shortestQ
	if queueType == "Boarding-Pass Check Queue" {
		fmt.Printf(
			"Client %d has started process. \n", c.ClientID)
		c.ClientStartTime = c.Env.GlobalTime
	}
	//fmt.Printf(
	//	"Client %d joined queue #%d, type %s. \n", c.ClientID, shortestQ.QueueID, queueType)
	return nil
}
