package main

import (
	"bitbucket.org/ChrisHan82/queuesimulator-engine/src/models"
	"fmt"
	rng "github.com/leesper/go_rng"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

func main() {
	clientsCount, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatal(err.Error())
	}
	scannerQueueCount, err := strconv.Atoi(os.Args[2])
	if err != nil {
		log.Fatal(err.Error())
	}
	serverCount, err := strconv.Atoi(os.Args[3])
	if err != nil {
		log.Fatal(err.Error())
	}

	env := &models.Environment{
		GlobalTime: 0,
	}
	clients := make([]*models.Client, 0)
	for i := 1; i <= clientsCount; i++ {
		clients = append(clients, &models.Client{
			Env: env,
			ClientID: i,
		})
	}
	idCheckQueues := make([]*models.Queue, 0)
	for i := 1; i <= 1; i++ {
		idCheckQueues = append(idCheckQueues, &models.Queue{
			Env: env,
			QueueID: i,
			QueueType: "Boarding-Pass Check Queue",
		})
	}
	scannerQueues := make([]*models.Queue, 0)
	for i := 1; i <= scannerQueueCount; i++ {
		scannerQueues = append(scannerQueues, &models.Queue{
			Env: env,
			QueueID: i,
			QueueType: "Scanner Queue",
		})
	}
	servers := make([]*models.Server, 0)
	for i := 1; i <= serverCount; i++ {
		servers = append(servers, &models.Server{
			Env: env,
			ServerID: i,
			ServerRecentIdleStartTime: 0,
		})
	}
	scanners := make([]*models.Scanner, 0)
	for i := 1; i <= scannerQueueCount; i++ {  // Needs to be the same #scannerQueues
		scanners = append(scanners, &models.Scanner{
			Env: env,
			ScannerID: i,
			ScannerRecentIdleStartTime: 0,
            ScannerBoundQueue: scannerQueues[i - 1],
		})
	}

	env.ClientsInEnv = clients
	env.ServersInEnv = servers
    env.ScannersInEnv = scanners
	env.IDCheckQueuesInEnv = idCheckQueues
	env.ScannerQueuesInEnv = scannerQueues

	// 4 Goroutines. 1 minute -> 1 second
	var totalWG sync.WaitGroup
	totalWG.Add(1)
	go func() {
		defer totalWG.Done()
		time.Sleep(time.Duration(clientsCount) * time.Second)
		totalWaitTime := 0
		for _, client := range env.ClientsInEnv {
			totalWaitTime += client.ClientEndTime - client.ClientStartTime
		}
		fmt.Printf("Result: Average wait time is %f.", float64(totalWaitTime / len(env.ClientsInEnv)))
	}()
	// 1st Goroutines: Global time starts ticking
	go func() {
		for true {
			time.Sleep(1 * time.Second)
			env.GlobalTime += 1
		}
	}()
	// 2nd Goroutines: Client starts coming
	go func() {
		for _, client := range env.ClientsInEnv {
			if err := client.JoinShortestQueue("Boarding-Pass Check Queue"); err != nil {
				log.Fatal(err.Error())
			}
			poissonRNG := rng.NewPoissonGenerator(time.Now().UnixNano())
			rn := int(poissonRNG.Poisson(5))
			nextInterval := int(float32(1000) / float32(rn))
			time.Sleep(time.Duration(nextInterval) * time.Millisecond)
		}
	}()
	// 3rd Goroutines: Server starts serving
	go func() {
		for true {
			time.Sleep(1 * time.Millisecond)
			for _, server := range env.ServersInEnv {
				time.Sleep(1 * time.Millisecond)
				go func() {
					if server.ServingClient == nil {
						if err := server.ServeLongestQueue(); err != nil {
							log.Fatal(err.Error())
						}
					}
				}()
			}
		}
	}()
	// 4th Goroutines: Scanner starts scanning
	go func() {
		for true {
			time.Sleep(1 * time.Millisecond)
			for _, scanner := range env.ScannersInEnv {
				time.Sleep(1 * time.Millisecond)
				go func() {
					if scanner.ScanningClient == nil {
						if err := scanner.ScanBoundQueue(); err != nil {
							log.Fatal(err.Error())
						}
					}
				}()
			}
		}
	}()
	totalWG.Wait()
}